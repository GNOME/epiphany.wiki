This is a step by step guide to prepare a new official release for distribution.

## Files which needs to be changed

1. Write NEWS file NEWS file needs to contain the most significant changes since the last release. Summarize steps if necessary
2. Bump version in meson.build Depending of what kind of build is prepared update major/minor/patch version: **(VERSION)**
3. Bump version in appdata.in The same **(VERSION)** also needs to be applied to the appdata.in file.

## GIT related changes

1. Push all updated version/release files to git
2. Create a GIT tag in gitlab 2.1 `git tag -a (VERSION)` Description: "Epiphany (VERSION)"
3. Push tag: `git push --follow-tags`
4. Copy NEWS version entry to tag release section

## Release archives

1. Create a release tar ball **(RELEASE_TARBALL)** in development environment with: `meson dist` This creates a release tar ball containing all the necessary dependencies
2. Upload tarball `scp (RELEASE_TARBALL) master.gnome.org:`
3. Log in to master.gnome.org `ssh master.gnome.org`
4. Install release tarball `ftpadmin install (RELEASE_TARBALL)`