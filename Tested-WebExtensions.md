Please only test WebExtensions on the latest `org.gnome.Epiphany.Devel` for now until 43 releases.

| Extension Name | Tested Version | Status | Tester | Notes |
| - | - | - | - | - |
| Bitwarden | 1.54.0 (firefox) | Partial | @pgriffis | The popup works and filling from it usually works. Background page hits issues and fails to create menus. i18n usage has minor string issues. |
